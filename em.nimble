# Package

version       = "0.1.0"
author        = "SteamFox"
description   = "Linear algebra with CG in mind."
license       = "MIT"

# Dependencies

requires "nim >= 0.14.3"

srcDir = "src"
bin = @["test"]
